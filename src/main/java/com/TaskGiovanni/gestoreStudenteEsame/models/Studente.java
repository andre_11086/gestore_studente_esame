package com.TaskGiovanni.gestoreStudenteEsame.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="studente")
public class Studente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="studente_id")
	private int idStudente;
	
	@Column
	private String matricola;
	@Column
	private String nome;
	@Column
	private String cognome;
	@Column
	private Date data_nascita;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "studente_esame",
			joinColumns = {@JoinColumn(name = "identificativo_studente", referencedColumnName="studente_id")}, 
			inverseJoinColumns = {@JoinColumn(name="identificativo_esame", referencedColumnName="id_esame")}
	)
	@JsonIgnoreProperties("studentiIscritti")
	private List<Esame> esamiDaSostenere;
	
	public Studente() {
		esamiDaSostenere = new ArrayList<Esame>();
	}

	public int getIdStudente() {
		return idStudente;
	}

	public void setIdStudente(int idStudente) {
		this.idStudente = idStudente;
	}

	public String getMatricola() {
		return matricola;
	}

	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public Date getData_nascita() {
		return data_nascita;
	}

	public void setData_nascita(Date data_nascita) {
		this.data_nascita = data_nascita;
	}

	public List<Esame> getEsamiDaSostenere() {
		return esamiDaSostenere;
	}

	public void setEsamiDaSostenere(List<Esame> esamiDaSostenere) {
		this.esamiDaSostenere = esamiDaSostenere;
	}

	public Studente(String matricola, String nome, String cognome, Date data_nascita) {
		super();
		this.matricola = matricola;
		this.nome = nome;
		this.cognome = cognome;
		this.data_nascita = data_nascita;
	}

	
	
	
}
