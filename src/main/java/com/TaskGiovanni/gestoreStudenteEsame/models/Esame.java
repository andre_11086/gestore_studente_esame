package com.TaskGiovanni.gestoreStudenteEsame.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="esame")
public class Esame {

	@Column
	private String nome;
	@Column
	private int crediti;
	@Column
	private Date data_esame;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private int id_esame;
	@Column
	private String codice_esame;
	
	@ManyToMany(mappedBy ="esamiDaSostenere", fetch = FetchType.EAGER)
	@JsonIgnoreProperties("esamiDaSostenere")
	private List<Studente> studentiIscritti;
	
	public Esame() {
		studentiIscritti = new ArrayList<Studente>();
	}

	public Esame(String nome, int crediti, Date dataEsame, String codice_esame) {
		super();
		this.nome = nome;
		this.crediti = crediti;
		this.data_esame = dataEsame;
		this.codice_esame = codice_esame;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getCrediti() {
		return crediti;
	}

	public void setCrediti(int crediti) {
		this.crediti = crediti;
	}

	public Date getDataEsame() {
		return data_esame;
	}

	public void setDataEsame(Date dataEsame) {
		this.data_esame = dataEsame;
	}

	public int getId_esame() {
		return id_esame;
	}

	public void setId_esame(int id_esame) {
		this.id_esame = id_esame;
	}

	public String getCodice_esame() {
		return codice_esame;
	}

	public void setCodice_esame(String codice_esame) {
		this.codice_esame = codice_esame;
	}

	public List<Studente> getStudentiIscritti() {
		return studentiIscritti;
	}

	public void setStudentiIscritti(List<Studente> studentiIscritti) {
		this.studentiIscritti = studentiIscritti;
	}

	
	
	
}
