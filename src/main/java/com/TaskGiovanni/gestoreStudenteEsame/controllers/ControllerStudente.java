package com.TaskGiovanni.gestoreStudenteEsame.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.TaskGiovanni.gestoreStudenteEsame.models.Esame;
import com.TaskGiovanni.gestoreStudenteEsame.models.Studente;
import com.TaskGiovanni.gestoreStudenteEsame.services.EsameServices;
import com.TaskGiovanni.gestoreStudenteEsame.services.StudenteServices;

@RestController
@RequestMapping("/studente")
public class ControllerStudente {
	@Autowired
	private StudenteServices services;
	@Autowired
	private EsameServices examServices;

	@PostMapping("/inserisciStudente")
	public Studente salvaStudente(@RequestBody Studente stud) {
		return services.save(stud);
	}
	
	@GetMapping("/trovaPerId")
	public Studente trovaPerId(int matricola) {
		return services.findById(matricola);
	}
	
	@GetMapping("/findByName")
	public Studente trovaPerId(String nome) {
		return services.findByName(nome);
	}
	
	@GetMapping("/findByNameAndSurname")
	public List<Studente> findByNameAndSurname(String nome, String cognome) {
		return services.findByNameAndSurname(nome, cognome);
	}
	
	@GetMapping("/likeByNameAndSurname")
	public List<Studente> likeByNameAndSurname(String nome, String cognome) {
		return services.likeByNameAndSurname(nome, cognome);
	}
	
	@GetMapping("/studenti")
	public List<Studente> findAll(){
		return services.findAll();
	}
	
	@DeleteMapping("/delete")
	public boolean deleteStudente(int matricola) {
		return services.delete(matricola);
	}
	
	@PutMapping("/update")
	public boolean aggiornaStudente(@RequestBody Studente stud) {
		return services.update(stud);
	}
	
	@GetMapping("/iscriviAdEsame")
	public Studente iscriviAdEsame(int idStudente, int idEsame) {
		Studente temp = services.findById(idStudente);
		Esame exam = examServices.findById(idEsame);
		temp.getEsamiDaSostenere().add(exam);
		services.update(temp);
		return temp;
	}
	
	@GetMapping("/listaEsami")
	public List<Esame> listaEsami(int idStudente){
		return services.findById(idStudente).getEsamiDaSostenere();
	}
	
}
