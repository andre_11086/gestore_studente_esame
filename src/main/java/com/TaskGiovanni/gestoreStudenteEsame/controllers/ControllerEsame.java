package com.TaskGiovanni.gestoreStudenteEsame.controllers;

import java.util.List;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.TaskGiovanni.gestoreStudenteEsame.models.Esame;
import com.TaskGiovanni.gestoreStudenteEsame.models.Studente;
import com.TaskGiovanni.gestoreStudenteEsame.services.EsameServices;

@RestController
@RequestMapping("/esame")
public class ControllerEsame {
	@Autowired
	private EsameServices service;
	
	@PostMapping("/inserisci")
	public Esame insertEsame(@RequestBody Esame exam) {
		return service.save(exam);
	}
	
	@GetMapping("/esameSingolo")
	public Esame esameSingolo(int id) {
		return service.findById(id);
	}
	
	@GetMapping("/listaEsami")
	public List<Esame> esami(){
		return service.findAll();
	}
	
	@DeleteMapping("/delete")
	public boolean delete(int id) {
		return service.delete(id);
	}
	
	@PutMapping("/update")
	public boolean update(@RequestBody Esame exam) {
		return service.update(exam);
	}
	
	@GetMapping("/listaIscritti")
	public List<Studente> listaIscritti(int idEsame){
		return service.findById(idEsame).getStudentiIscritti();
	}
}
