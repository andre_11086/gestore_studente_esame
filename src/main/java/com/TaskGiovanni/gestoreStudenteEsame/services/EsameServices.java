package com.TaskGiovanni.gestoreStudenteEsame.services;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.TaskGiovanni.gestoreStudenteEsame.models.Esame;

@Service
public class EsameServices {
	@Autowired
	private EntityManager entMan;

	private Session getSessione() {
		return entMan.unwrap(Session.class);
	}
	
	public Esame save(Esame obj) {
		Esame temp = new Esame();
		temp.setNome(obj.getNome());
		temp.setCrediti(obj.getCrediti());
		temp.setDataEsame(obj.getDataEsame());
		temp.setCodice_esame(obj.getCodice_esame());
		temp.setStudentiIscritti(obj.getStudentiIscritti());
		
		try {
			getSessione().save(temp);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

		return temp;
	}
	
	public Esame findById(int id) {
		return (Esame) getSessione().createCriteria(Esame.class)
				.add(Restrictions.eqOrIsNull("id_esame", id))
				.uniqueResult();
	}
	
	public Esame findByName(String exam) {
		return (Esame) getSessione().createCriteria(Esame.class)
				.add(Restrictions.eqOrIsNull("nome", exam))
				.uniqueResult();
	}
	
	public List<Esame> findByDate(Date exam) {
		return (List<Esame>) getSessione().createCriteria(Esame.class)
				.add(Restrictions.eqOrIsNull("data_esame", exam))
				.list();
	}
	
	public List<Esame> findAll(){
		return getSessione().createCriteria(Esame.class).list();
	}
	
	@Transactional
	public boolean delete(int id) {
		try {
			getSessione().delete(getSessione().load(Esame.class, id));
			getSessione().flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return false;
	}
	
	@Transactional
	public boolean update(Esame exam) {
		try {
			Esame temp = getSessione().load(Esame.class, exam.getId_esame());
			if(temp != null) {
				temp.setNome(exam.getNome());
				temp.setDataEsame(exam.getDataEsame());
				temp.setCrediti(exam.getCrediti());
				
				getSessione().update(temp);
				getSessione().flush();
				
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return false;
	}
}
