package com.TaskGiovanni.gestoreStudenteEsame.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.TaskGiovanni.gestoreStudenteEsame.models.Studente;

@Service
public class StudenteServices {
	@Autowired
	private EntityManager entMan;

	private Session getSessione() {
		return entMan.unwrap(Session.class);
	}

	public Studente save(Studente stud) {
		Studente temp = new Studente();
		temp.setNome(stud.getNome());
		temp.setCognome(stud.getCognome());
		temp.setData_nascita(stud.getData_nascita());
		temp.setMatricola(stud.getMatricola());

		try {
			getSessione().save(temp);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

		return temp;
	}

	public Studente findById(int id) {
		return (Studente) getSessione().createCriteria(Studente.class).add(Restrictions.eqOrIsNull("idStudente", id))
				.uniqueResult();
	}

	public Studente findByName(String name) {
		return (Studente) getSessione().createCriteria(Studente.class).add(Restrictions.eqOrIsNull("nome", name))
				.uniqueResult();
	}

	public List<Studente> findByNameAndSurname(String name, String surname) {
		return (List<Studente>) getSessione().createCriteria(Studente.class).add(Restrictions.eqOrIsNull("nome", name))
				.add(Restrictions.eqOrIsNull("cognome", surname)).list();
	}

	public List<Studente> likeByNameAndSurname(String name, String surname) {
		return (List<Studente>) getSessione().createCriteria(Studente.class)
				.add(Restrictions.like("nome", name+"%", MatchMode.ANYWHERE))
				.add(Restrictions.like("cognome", surname+"%", MatchMode.ANYWHERE)).list();
	}

	public List<Studente> findAll() {
		return getSessione().createCriteria(Studente.class).list();
	}

	@Transactional
	public boolean delete(int id) {
		try {
			getSessione().delete(getSessione().load(Studente.class, id));
			getSessione().flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return false;
	}

	@Transactional
	public boolean update(Studente stud) {
		try {
			Studente temp = getSessione().load(Studente.class, stud.getIdStudente());
			if (temp != null) {
				temp.setNome(stud.getNome());
				temp.setCognome(stud.getCognome());
				temp.setData_nascita(stud.getData_nascita());

				getSessione().update(temp);
				getSessione().flush();

				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return false;
	}

}
