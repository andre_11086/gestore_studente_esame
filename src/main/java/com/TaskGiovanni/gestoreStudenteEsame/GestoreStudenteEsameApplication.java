package com.TaskGiovanni.gestoreStudenteEsame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestoreStudenteEsameApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestoreStudenteEsameApplication.class, args);
	}

}
